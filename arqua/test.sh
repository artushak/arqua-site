#!/bin/sh

echo "Running tests..."
# python ./manage.py test
MEDIA_ROOT="arqua/media_test" coverage run ./arqua/manage.py test userprofiles.tests videohosting.tests telechat.tests
rm -rf ./arqua/media_test/*
echo "Generating coverage report..."
rm -rf ./coverage/
coverage html --directory=./coverage/
