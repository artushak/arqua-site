# -*- coding: utf-8 -*-
"""Django DB models for `telechat`."""
from django.db import models
from django.utils import timezone
from django.utils.html import conditional_escape
from rest_framework import (
    serializers, viewsets, permissions, filters, pagination
)
import django_filters.rest_framework as drf_filters

from userprofiles.models import (
    UserProfile, UserProfileSerializer, get_user_profile_current_object
)


class ChatMessage(models.Model):
    """Chat message."""

    pub_time = models.DateTimeField('Время отправки',
                                    null=True,
                                    default=timezone.now,
                                    blank=True)
    author = models.ForeignKey(UserProfile,
                               verbose_name='Автор',
                               null=True,
                               on_delete=models.SET_NULL)
    is_deleted = models.BooleanField(verbose_name='Удалено', default=False,
                                     blank=True)
    text = models.TextField(verbose_name='Текст')

    def __str__(self):
        """Return string representation."""
        return '#{} {}'.format(self.pk, self.text)

    class Meta:
        """Metaclass."""

        verbose_name = 'сообщение чата'
        verbose_name_plural = 'сообщение чата'
        permissions = (
            ('can_send_message', 'Может отправлять сообщения'),
        )


class ChatMessageSerializer(serializers.ModelSerializer):
    """Serializer for Django `User`."""

    author = UserProfileSerializer(read_only=True)

    def validate(self, data):
        """Validate serialization data."""
        if self.context['view'].action in ['create']:
            if 'author' not in data:
                data['author'] = get_user_profile_current_object(
                    self.context['request']
                )
        data['text'] = conditional_escape(data['text'])
        return data

    class Meta:
        """Metaclass."""

        model = ChatMessage
        fields = (
            'pk', 'pub_time', 'author', 'text'
        )
        read_only_fields = ('pub_time', 'author')


class ChatMessageAPIPermissions(permissions.BasePermission):
    """Permissions for `ChatMessage` view set."""

    def has_permission(self, request, view):
        """Return `True` if permission is granted, `False` otherwise."""
        if view.action == 'create':
            if not request.user or (not request.user.is_authenticated):
                return False
            return request.user.has_perm('telechat.can_send_message')
        if view.action in ['list', 'retrieve']:
            return True

        return False


class ChatMessageFilter(drf_filters.FilterSet):
    """API filter set for `ChatMessage` view set."""

    pub_time_min = drf_filters.IsoDateTimeFilter(
        field_name='pub_time', lookup_expr='gte'
    )
    pub_time_after = drf_filters.IsoDateTimeFilter(
        field_name='pub_time', lookup_expr='gt'
    )
    pub_time_max = drf_filters.IsoDateTimeFilter(
        field_name='pub_time', lookup_expr='lte'
    )
    pub_time_before = drf_filters.IsoDateTimeFilter(
        field_name='pub_time', lookup_expr='lt'
    )

    class Meta:
        """Metaclass."""

        model = ChatMessage
        fields = ('pub_time',)


class ChatMessageViewSet(viewsets.ModelViewSet):
    """View set for `ChatMessage`."""

    queryset = ChatMessage.objects.all()
    serializer_class = ChatMessageSerializer
    permission_classes = (ChatMessageAPIPermissions | permissions.IsAdminUser,)
    filter_backends = (
        drf_filters.DjangoFilterBackend,
        filters.OrderingFilter
    )
    ordering_fields = ('pk', 'pub_time')
    pagination_class = pagination.LimitOffsetPagination
    filterset_class = ChatMessageFilter

    def get_queryset(self):
        """Return queryset for view set."""
        return ChatMessage.objects.filter(is_deleted=False)
