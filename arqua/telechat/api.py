# -*- coding: utf-8 -*-
"""Django REST API configuration for `telechat`."""
from rest_framework import routers

from telechat.models import ChatMessageViewSet

router = routers.DefaultRouter()
router.register(r'chat-messages', ChatMessageViewSet)
