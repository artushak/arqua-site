# -*- coding: utf-8 -*-
"""Django application `telechat`."""
from django.apps import AppConfig
from django.urls import reverse_lazy


class TelechatConfig(AppConfig):
    """Django application configuration for `telechat`."""

    name = 'telechat'
    verbose_name = 'чат'


page_path_base = [
    ('тчк', reverse_lazy('index')),
    ('телечат', reverse_lazy('telechat:chat')),
]
