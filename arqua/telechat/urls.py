# -*- coding: utf-8 -*-
"""Django URL configuration for `telechat`."""
from django.conf.urls import url, include

from telechat import views, api


app_name = 'telechat'

urlpatterns = [
    url(r'^chat$', views.ChatView.as_view(), name='chat'),
    url(r'^api/', include(api.router.urls)),
]
