# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-01 17:54
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('userprofiles', '0010_auto_20171201_0955'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChatMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pub_time', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Время отправки')),
                ('is_deleted', models.BooleanField(verbose_name='Удалено')),
                ('text', models.TextField(verbose_name='Текст')),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='userprofiles.UserProfile', verbose_name='Автор')),
            ],
            options={
                'verbose_name': 'сообщение чата',
                'verbose_name_plural': 'сообщение чата',
                'permissions': (('can_send_message', 'Может отправлять сообщения'),),
            },
        ),
    ]
