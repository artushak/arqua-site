# -*- coding: utf-8 -*-
"""Django views for `telechat`."""
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_GET
from django.views.generic.base import TemplateView

from telechat.apps import page_path_base


@method_decorator(require_GET, name='dispatch')
class ChatView(TemplateView):
    """Chat view."""

    template_name = 'telechat/chat.html'

    def get_context_data(self, **kwargs):
        """Get context data for view."""
        context = super(ChatView, self).get_context_data(**kwargs)
        context['can_send_message'] = \
            self.request.user.has_perm('telechat.can_send_message')
        context['page_path'] = page_path_base
        return context
