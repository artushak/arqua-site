# -*- coding: utf-8 -*-
"""Django administration configuration for `telechat`."""
from django.contrib import admin

from telechat.models import ChatMessage


class ChatMessageAdmin(admin.ModelAdmin):
    """Administration class for `ChatMessage`."""

    model = ChatMessage


admin.site.register(ChatMessage, ChatMessageAdmin)
