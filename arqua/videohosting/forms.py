# -*- coding: utf-8 -*-
"""Django forms for `videohosting`."""
from django import forms

from arqua.utils import form_with_helper
from videohosting.models import Video


@form_with_helper
class VideoCreateForm(forms.ModelForm):
    """Form for creating video (without uploading file)."""

    class Meta:
        """Metaclass."""

        model = Video
        fields = ['video_name', 'description', 'poster']
