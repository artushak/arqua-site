# -*- coding: utf-8 -*-
"""Django template tags and filters for `userprofiles`."""
from django.template import Library


register = Library()


@register.filter()
def is_liker(liker, target):
    """
    Return `True` if `liker` has liked video `target`.

    `liker` should be Django `User`, `target` should be `Video`.
    """
    return bool(target.likes.filter(user__pk=liker.pk))
