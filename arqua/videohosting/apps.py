# -*- coding: utf-8 -*-
"""Django application `videohosting`."""
from django.apps import AppConfig
from django.urls import reverse_lazy


class VideohostingConfig(AppConfig):
    """Django application configuration for `videohosting`."""

    name = 'videohosting'
    verbose_name = 'видеохостинг'


page_path_base = [
    ('тчк', reverse_lazy('index')),
    ('видеохостинг', None),
]
