# -*- coding: utf-8 -*-
"""Django views for `videohosting`."""
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.http.response import HttpResponseRedirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_GET
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView
from private_storage.views import PrivateStorageDetailView

from userprofiles.models import get_user_profile_current_object
from videohosting.models import Video, restrict_videos, get_video_access
from videohosting.forms import VideoCreateForm
from videohosting.apps import page_path_base


@method_decorator(require_GET, name='dispatch')
class VideoRecentView(ListView):
    """Recent videos view."""

    template_name = 'videohosting/video_recent.html'

    def get_context_data(self, **kwargs):
        """Return context data for view."""
        context = super(VideoRecentView, self).get_context_data(**kwargs)
        page_name = 'новые видео'
        page_url = reverse('videohosting:video_recent')
        context['page_path'] = page_path_base + [(page_name, page_url)]
        return context

    def get_queryset(self):
        """Return queryset for view."""
        qs = Video.objects.order_by('-pub_time')
        return restrict_videos(qs, self.request)[:10]


@method_decorator(require_GET, name='dispatch')
class VideoSearchView(ListView):
    """Video search view."""

    template_name = 'videohosting/video_search.html'

    def dispatch(self, *args, **kwargs):
        if 'query' not in self.request.GET:
            raise Http404
        return super(VideoSearchView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        """Return context data for view."""
        context = super(VideoSearchView, self).get_context_data(**kwargs)
        search_query = self.request.GET['query']
        context['search_query'] = search_query
        context['pagename'] = '.поиск-видео:{}'.format(search_query)
        return context

    def get_queryset(self):
        """Return queryset for view."""
        qs = Video.objects.filter(
            video_name__contains=self.request.GET['query']
        ).order_by('-pub_time')
        return restrict_videos(qs, self.request)[:10]


@method_decorator(require_GET, name='dispatch')
class VideoView(DetailView):
    """Video watch view."""

    model = Video

    template_name = 'videohosting/video.html'

    def get_context_data(self, **kwargs):
        """Return context data for view."""
        context = super(VideoView, self).get_context_data(**kwargs)
        context['can_comment'] = \
            self.request.user.has_perm('videohosting.can_add_video_comments')
        context['can_like'] = \
            self.request.user.has_perm('videohosting.can_add_video_likes')
        context['can_subscribe'] = \
            self.request.user.has_perm('userprofiles.can_subscribe')
        page_name = 'видео:{}'.format(
            context['object'].video_name
        )
        page_url = reverse('videohosting:video', args=(self.object.pk,))
        context['page_path'] = page_path_base + [(page_name, page_url)]
        return context

    def get_object(self, queryset=None):
        """Return video object for view."""
        obj = super(VideoView, self).get_object(queryset=queryset)
        if not get_video_access(obj, self.request):
            raise PermissionDenied()
        return obj

    def get(self, request, *args, **kwargs):
        """Process request."""
        self.object = self.get_object()
        if not self.object.video_file:
            return HttpResponseRedirect(reverse('videohosting:video_edit',
                                                args=(self.object.pk, )))
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class VideoCreateView(PermissionRequiredMixin, CreateView):
    """Video creation view."""

    permission_required = 'videohosting.add_video'
    template_name = 'videohosting/video_create.html'
    model = Video
    form_class = VideoCreateForm

    def get_context_data(self, **kwargs):
        """Return context data for view."""
        context = super(VideoCreateView, self).get_context_data(**kwargs)
        page_name = 'добавление видео'
        page_url = reverse('videohosting:video_create')
        context['page_path'] = page_path_base + [(page_name, page_url)]
        return context

    def form_valid(self, form):
        response = super(VideoCreateView, self).form_valid(form)
        self.object.author = get_user_profile_current_object(self.request)
        self.object.save()
        return response


@method_decorator(require_GET, name='dispatch')
class VideoEditView(DetailView):
    """Video editing view."""

    model = Video

    template_name = 'videohosting/video_edit.html'

    def get_context_data(self, **kwargs):
        """Return context data for view."""
        context = super(VideoEditView, self).get_context_data(**kwargs)
        page_name = 'редактирование видео:{}'.format(
            context['object'].video_name
        )
        page_url = reverse('videohosting:video_create')
        context['page_path'] = page_path_base + [(page_name, page_url)]
        return context

    def get_object(self, queryset=None):
        """Return video object for view."""
        obj = super(VideoEditView, self).get_object(queryset=queryset)
        if obj.author is None:
            raise PermissionDenied()
        if obj.author.user != self.request.user:
            raise PermissionDenied()
        return obj


class VideoFileDownloadView(PrivateStorageDetailView):
    model = Video
    model_file_field = 'video_file'

    def get_queryset(self):
        return restrict_videos(super().get_queryset(), self.request)


class VideoPosterDownloadView(PrivateStorageDetailView):
    model = Video
    model_file_field = 'Poster'

    def get_queryset(self):
        return restrict_videos(super().get_queryset(), self.request)
