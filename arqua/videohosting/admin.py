# -*- coding: utf-8 -*-
"""Django administration configuration for `videohosting`."""
from django.contrib import admin

from videohosting.models import Video, VideoComment


class VideoAdmin(admin.ModelAdmin):
    """Administration class for `Video`."""

    model = Video


class VideoCommentAdmin(admin.ModelAdmin):
    """Administration class for `VideoComment`."""

    model = VideoComment


admin.site.register(Video, VideoAdmin)
admin.site.register(VideoComment, VideoCommentAdmin)
