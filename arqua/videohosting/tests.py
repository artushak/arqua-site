"""Tests for `videohosting`."""
import os
import pytz

from django.test import Client, TestCase
from django.contrib.auth.models import Group, User, Permission
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.utils.dateparse import parse_datetime
from selenium.webdriver import Firefox as WebDriver

from userprofiles.models import get_user_profile_object_from_user, InviteCode
from videohosting.models import Video


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TEST_FILES_DIR = os.path.join(BASE_DIR, 'testfiles')

IMAGE_FILE_1 = os.path.join(TEST_FILES_DIR, 'img1.png')
IMAGE_FILE_2 = os.path.join(TEST_FILES_DIR, 'img2.png')
IMAGE_FILE_3 = os.path.join(TEST_FILES_DIR, 'img3.jpg')
VIDEO_FILE_1 = os.path.join(TEST_FILES_DIR, 'video1.webm')


class MainTestCase(TestCase):
    """Tests for simple functionality."""

    def setUp(self):
        """Initialize objects before testing."""
        self.user_john = User.objects.create_user(
            'john', 'lennon@thebeatles.com', 'johnpassword'
        )
        self.user_john.is_superuser = True
        self.user_john.save()
        self.user_profile_john = get_user_profile_object_from_user(
            self.user_john
        )

        self.video1 = Video.objects.create(
            video_name='VIDEO1', description='DESCRIPTION1',
            author=self.user_profile_john,
            pub_time=pytz.timezone("Asia/Tokyo").localize(
                parse_datetime('2014-04-01 10:15')
            )
        )

    def test_web(self):
        c = Client()
        response1 = c.post(
            '/accounts/login',
            {'username': 'john', 'password': 'johnpassword'},
            follow=True
        )
        self.assertEquals(response1.status_code, 200)
        print()
        response2 = c.get('/videohosting/video_recent')
        video1_url = '"/videohosting/video/{}"'.format(self.video1.pk)
        self.assertNotContains(response2, video1_url)


class SeleniumTestCase(StaticLiveServerTestCase):
    """Selenium tests."""

    @classmethod
    def setUpClass(cls):
        """Initialize class before testing."""
        super().setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    def setUp(self):
        """Initialize objects before testing."""
        self.user_john = User.objects.create_user(
            'john', 'lennon@thebeatles.com',  'johnpassword'
        )
        self.user_john.is_superuser = True
        self.user_john.save()

        self.group_confirmed = Group.objects.create(name='confirmed')
        permissions = Permission.objects.filter(
            codename__in=[
                'can_subscribe', 'can_add_video_likes'
            ]
        )
        self.group_confirmed.permissions.add(*permissions)

        InviteCode.objects.create(
            username='radowid', code_text='taburetka'
        )

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def waitAndLocateElement(self, xpath, max_try_num=500):
        jquery_active = True
        try_num = 0
        while jquery_active:
            if try_num > max_try_num:
                raise Exception(
                    (f'Timeout while trying to locate "{xpath}"'
                     f'(maximum {try_num} tries)')
                )
            jquery_active = bool(
                self.selenium.execute_script('return jQuery.active;')
            )
            if jquery_active:
                self.selenium.implicitly_wait(1)
            try_num += 1
        return self.selenium.find_element_by_xpath(xpath)

    def test_web(self):
        self.selenium.get('{}{}'.format(
            self.live_server_url, '/accounts/login/'
        ))
        self.selenium.find_element_by_name('username').send_keys('john')
        self.selenium.find_element_by_name('password').send_keys(
            'johnpassword'
        )
        self.selenium.find_element_by_xpath('//*[@value="Войти"]').click()

        self.waitAndLocateElement(
            '//a[contains(text(), "видеохостинг")]'
        ).click()

        self.waitAndLocateElement(
            '//a[contains(text(), "добавление видео")]'
        ).click()

        self.selenium.find_element_by_name('video_name').send_keys(
            'ТОП 10 ЖЁСТКИХ ФЕЙЛОВ ЗАЦЕПЕРОВ'
        )
        self.selenium.find_element_by_name('description').send_keys(
            '18+ шок-контент максимальный репост'
        )
        self.selenium.find_element_by_name('poster').send_keys(IMAGE_FILE_3)
        self.waitAndLocateElement(
            '//*[@value="Сохранить"]'
        ).click()

        self.selenium.find_element_by_name('video_file').send_keys(
            VIDEO_FILE_1
        )
        self.waitAndLocateElement(
            '//*[contains(text(), "Загрузить")]'
        ).click()

        self.waitAndLocateElement(
            '//a[contains(text(), "видеохостинг")]'
        ).click()

        self.waitAndLocateElement(
            '//a[contains(text(), "ТОП 10 ЖЁСТКИХ ФЕЙЛОВ ЗАЦЕПЕРОВ")]'
        ).click()

        self.waitAndLocateElement(
            '//*[contains(text(), "поставить лайк")]'
        ).click()
        self.waitAndLocateElement(
            '//*[contains(text(), "отменить лайк")]'
        ).click()
        self.selenium.find_element_by_id('video-comment-add-text').send_keys(
            'Крутой видос, но лучше сделай обзор на Копатель!'
        )
        self.waitAndLocateElement(
            '//*[contains(text(), "Отправить")]'
        ).click()

        self.waitAndLocateElement(
            '//a[contains(text(), "john")]'
        ).click()

        self.waitAndLocateElement(
            '//*[contains(text(), "подписаться")]'
        ).click()
        self.waitAndLocateElement(
            '//*[contains(text(), "отписаться")]'
        ).click()

        self.waitAndLocateElement(
            '//a[contains(text(), "телечат")]'
        ).click()

        self.selenium.find_element_by_id('chat-message-add-text').send_keys(
            'Кожда же этот Джон наконец-то сделает обзор на Копатель...'
        )
        self.waitAndLocateElement(
            '//*[contains(text(), "Отправить")]'
        ).click()
        self.waitAndLocateElement(
            '//*[contains(text(), "Копатель")]'
        ).click()

        self.waitAndLocateElement(
            '//a[contains(text(), "выход")]'
        ).click()

        self.waitAndLocateElement(
            '//a[contains(text(), "регистрация")]'
        ).click()

        self.selenium.find_element_by_name('username').send_keys(
            'radowid'
        )
        self.selenium.find_element_by_name('email').send_keys(
            'radovid@gov.redania'
        )
        self.selenium.find_element_by_name('password1').send_keys(
            'filippaidinafig127'
        )
        self.selenium.find_element_by_name('password2').send_keys(
            'filippaidinafig127'
        )
        self.selenium.find_element_by_name('invite_code').send_keys(
            'taburetka'
        )
        self.waitAndLocateElement(
            '//*[@value="Зарегистрироваться"]'
        ).click()

        User.objects.get(username='radowid').groups.add(self.group_confirmed)

        self.waitAndLocateElement(
            '//a[contains(text(), "видеохостинг")]'
        ).click()

        self.waitAndLocateElement(
            '//a[contains(text(), "ТОП 10 ЖЁСТКИХ ФЕЙЛОВ ЗАЦЕПЕРОВ")]'
        ).click()

        self.waitAndLocateElement(
            '//*[contains(text(), "поставить лайк")]'
        ).click()
        self.waitAndLocateElement(
            '//a[contains(text(), "john")]'
        ).click()
        self.waitAndLocateElement(
            '//*[contains(text(), "подписаться")]'
        ).click()

        self.waitAndLocateElement(
            '//a[contains(text(), "смена пароля")]'
        ).click()
        self.selenium.find_element_by_name('old_password').send_keys(
            'filippaidinafig127'
        )
        self.selenium.find_element_by_name('new_password1').send_keys(
            'filippaidinafig129'
        )
        self.selenium.find_element_by_name('new_password2').send_keys(
            'filippaidinafig129'
        )
        self.waitAndLocateElement(
            '//*[@value="Сменить"]'
        ).click()

        self.waitAndLocateElement(
            '//a[contains(text(), "выход")]'
        ).click()

        self.waitAndLocateElement(
            '//a[contains(text(), "телечат")]'
        ).click()
        self.waitAndLocateElement(
            '//*[contains(text(), "Копатель")]'
        ).click()
