# -*- coding: utf-8 -*-
"""Django DB models for `videohosting`."""
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.html import conditional_escape
import private_storage.fields
from rest_framework import (
    decorators, filters, pagination, parsers, permissions, response,
    serializers, status, viewsets
)
import django_filters.rest_framework as drf_filters

from userprofiles.models import (
    UserProfile, UserProfileSerializer,
    get_user_profile_current_object, get_user_profile_object_from_user
)


class Video(models.Model):
    """Video."""

    video_name = models.CharField(
        'Название', max_length=512
    )
    description = models.TextField(
        blank=True, verbose_name='Описание'
    )
    author = models.ForeignKey(
        UserProfile, verbose_name='Автор', null=True,
        on_delete=models.SET_NULL
    )
    video_file = private_storage.fields.PrivateFileField(
        upload_to='protected/videos/', verbose_name='Файл', null=True,
        content_types=['video/mp4', 'video/webm'],
        blank=True
    )
    poster = private_storage.fields.PrivateFileField(
        upload_to='protected/video_posters/', verbose_name='Обложка',
        null=True,
        content_types=[
            'image/png', 'image/jpeg', 'image/bmp', 'image/svg+xml'
        ]
    )
    pub_time = models.DateTimeField(
        'Время отправки', null=True, default=timezone.now, blank=True
    )
    likes = models.ManyToManyField(
        UserProfile, verbose_name='Лайкнувшие', related_name='liked_videos',
        blank=True
    )

    STATUS_PUBLIC = 'PUBLIC'
    STATUS_RESTRICTED = 'RESTRICTED'
    STATUS_BANNED = 'BANNED'
    restrictions = models.CharField('Уровень доступа',
                                    max_length=64,
                                    default=STATUS_PUBLIC,
                                    choices=[
                                        (STATUS_PUBLIC, 'Доступно всем'),
                                        (STATUS_RESTRICTED,
                                         'Ограниченный доступ'),
                                        (STATUS_BANNED, 'Заблокировано'),
                                    ])

    def detail_url(self):
        """Return URL of API method to get video data."""
        return reverse('videohosting:video-detail', args=[self.pk])

    def like_url(self):
        """Return URL of API method to like this video."""
        return reverse('videohosting:video-like', args=[self.pk])

    def unlike_url(self):
        """Return URL of API method to remove like from this video."""
        return reverse('videohosting:video-unlike', args=[self.pk])

    def get_absolute_url(self):
        """Return URL of video."""
        return reverse('videohosting:video', args=(self.pk, ))

    def __str__(self):
        """Return string representation."""
        return '#{} {}'.format(self.pk, self.video_name)

    class Meta:
        """Metaclass."""

        verbose_name = 'видеоролик'
        verbose_name_plural = 'видеоролики'
        permissions = (
            ('can_watch_restricted',
             'Может смотреть видеоролики с ограниченным доступом'),
            ('can_watch_banned', 'Может смотреть заблокированные видеоролики'),
            ('can_add_video_likes', 'Может ставить лайки к видеороликам'),
        )


def restrict_videos(qs, request):
    """Exclude from queryset videos which user can not view."""
    if not request.user.has_perm('videohosting.can_watch_restricted'):
        qs = qs.exclude(restrictions=Video.STATUS_RESTRICTED)
    if not request.user.has_perm('videohosting.can_watch_banned'):
        qs = qs.exclude(restrictions=Video.STATUS_BANNED)
    exclude_filter = models.Q(video_file='')
    if request.user.is_authenticated:
        exclude_filter &= ~models.Q(author__user=request.user)
    qs = qs.exclude(exclude_filter)
    return qs


def get_video_access(obj, request):
    """Return `True` if user has right to view video, otherwise `False`."""
    if obj.restrictions == Video.STATUS_RESTRICTED:
        return request.user.has_perm('videohosting.can_watch_restricted')
    if obj.restrictions == Video.STATUS_BANNED:
        return request.user.has_perm('videohosting.can_watch_banned')
    return True


class VideoSerializer(serializers.ModelSerializer):
    """Serializer for `Video`."""

    author = UserProfileSerializer(read_only=True)
    absolute_url = serializers.CharField(
        source='get_absolute_url', read_only=True
    )
    is_liked = serializers.SerializerMethodField()

    def get_is_liked(self, obj):
        """Return `True` if request user is subscribed to `obj`."""
        request_user = self.context['request'].user
        return bool(obj.likes.filter(user__pk=request_user.pk))

    class Meta:
        """Metaclass."""

        model = Video
        fields = (
            'pk', 'video_name', 'description', 'author', 'absolute_url',
            'pub_time', 'video_file', 'poster', 'is_liked'
        )
        read_only_fields = (
            'author', 'pub_time'
        )


class VideoAPIPermissions(permissions.BasePermission):
    """Permissions for `Video` view set."""

    def has_object_permission(self, request, view, obj):
        """Return `True` if permission is granted, `False` otherwise."""
        if not get_video_access(obj, request):
            return False

        if view.action in ['like', 'unlike']:
            if not request.user or (not request.user.is_authenticated):
                return False
            return request.user.has_perm('videohosting.can_add_video_likes')

        return False


class VideoViewSet(viewsets.ModelViewSet):
    """View set for `Video`."""

    queryset = Video.objects.all()
    serializer_class = VideoSerializer
    permission_classes = (VideoAPIPermissions | permissions.IsAdminUser,)

    def get_queryset(self):
        """Return queryset for view set."""
        return restrict_videos(Video.objects.all(), self.request)

    @decorators.action(detail=True, methods=('put',))
    def upload(
        self, request, pk=None, parser_classes=(parsers.FileUploadParser,)
    ):
        """Upload video file."""
        if 'video_file' not in request.FILES:
            return response.Response({}, status=status.HTTP_400_BAD_REQUEST)
        video_file = request.FILES['video_file']
        video = self.get_object()
        video.video_file = video_file
        video.save()
        return response.Response({})

    @decorators.action(detail=True, methods=('post',))
    def like(self, request, pk=None):
        """Add like from request user to this video."""
        video = self.get_object()
        video.likes.add(
            get_user_profile_object_from_user(request.user)
        )
        video.save()
        serializer = self.get_serializer(video)
        return response.Response(serializer.data)

    @decorators.action(detail=True, methods=('post',))
    def unlike(self, request, pk=None):
        """Remove like from request user to this video."""
        video = self.get_object()
        video.likes.remove(
            get_user_profile_object_from_user(request.user)
        )
        video.save()
        serializer = self.get_serializer(video)
        return response.Response(serializer.data)


class VideoComment(models.Model):
    """Comment for video."""

    author = models.ForeignKey(UserProfile,
                               verbose_name='Автор',
                               null=True,
                               on_delete=models.SET_NULL)
    text = models.TextField('Текст')
    pub_time = models.DateTimeField('Время отправки',
                                    null=True,
                                    default=timezone.now,
                                    blank=True)
    video = models.ForeignKey(Video,
                              verbose_name='Видеоролик',
                              null=True,
                              on_delete=models.CASCADE)
    is_deleted = models.BooleanField(verbose_name='Удалено', default=False,
                                     blank=True)

    def __str__(self):
        """Return string representation."""
        return '#{} к видео {}'.format(self.pk, self.video)

    class Meta:
        """Metaclass."""

        verbose_name = 'комментарий к видеоролику'
        verbose_name_plural = 'комментарии к видеороликам'
        permissions = (
            ('can_add_video_comments',
             'Может добавлять комментарии к видеороликам'),
        )


class VideoCommentSerializer(serializers.ModelSerializer):
    """Serializer for `VideoComment`."""

    author = UserProfileSerializer(read_only=True)
    video = serializers.PrimaryKeyRelatedField(
        many=False, queryset=Video.objects.all(), required=True
    )

    def to_internal_value(self, data):
        data = super(VideoCommentSerializer, self).to_internal_value(data)
        if self.instance:
            for x in self.create_only_fields:
                data.pop(x)
        return data

    def validate(self, data):
        """Validate serialization data."""
        if self.context['view'].action in ['create']:
            if 'author' not in data:
                data['author'] = get_user_profile_current_object(
                    self.context['request']
                )
        data['text'] = conditional_escape(data['text'])
        return data

    class Meta:
        """Metaclass."""

        model = VideoComment
        fields = (
            'pk', 'pub_time', 'author', 'text', 'video'
        )
        read_only_fields = ('pub_time', 'author')
        create_only_fields = ('video',)


class VideoCommentAPIPermissions(permissions.BasePermission):
    """Permissions for `VideoComment` view set."""

    def has_permission(self, request, view):
        """Return `True` if permission is granted, `False` otherwise."""
        if view.action == 'create':
            if not request.user or (not request.user.is_authenticated):
                return False
            return request.user.has_perm('telechat.can_add_video_comments')
        if view.action in ['list', 'retrieve']:
            return True

        return False


class VideoCommentFilter(drf_filters.FilterSet):
    """API filter set for `VideoComment` view set."""

    pub_time_min = drf_filters.IsoDateTimeFilter(
        field_name='pub_time', lookup_expr='gte'
    )
    pub_time_after = drf_filters.IsoDateTimeFilter(
        field_name='pub_time', lookup_expr='gt'
    )
    pub_time_max = drf_filters.IsoDateTimeFilter(
        field_name='pub_time', lookup_expr='lte'
    )
    pub_time_before = drf_filters.IsoDateTimeFilter(
        field_name='pub_time', lookup_expr='lt'
    )
    video = drf_filters.ModelChoiceFilter(
        field_name='video', queryset=Video.objects.all()
    )

    class Meta:
        """Metaclass."""

        model = VideoComment
        fields = ('pub_time', 'video')


class VideoCommentViewSet(viewsets.ModelViewSet):
    """View set for `VideoComment`."""

    queryset = VideoComment.objects.all()
    serializer_class = VideoCommentSerializer
    permission_classes = (VideoCommentAPIPermissions |
                          permissions.IsAdminUser,)
    filter_backends = (
        drf_filters.DjangoFilterBackend,
        filters.OrderingFilter
    )
    ordering_fields = ('pk', 'pub_time')
    pagination_class = pagination.LimitOffsetPagination
    filterset_class = VideoCommentFilter
