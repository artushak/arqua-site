# -*- coding: utf-8 -*-
"""Django REST API configuration for `videohosting`."""
from rest_framework import routers

from videohosting.models import VideoViewSet, VideoCommentViewSet

router = routers.DefaultRouter()
router.register(r'videos', VideoViewSet)
router.register(r'video-comments', VideoCommentViewSet)
