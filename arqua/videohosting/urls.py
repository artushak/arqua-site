# -*- coding: utf-8 -*-
"""Django URL configuration for `videohosting`."""
from django.conf.urls import url, include
from django.views.generic import RedirectView

from videohosting import views, api


app_name = 'videohosting'
urlpatterns = [
    url(r'^video_recent$', views.VideoRecentView.as_view(),
        name='video_recent'),
    url(r'^video_search$', views.VideoSearchView.as_view(),
        name='video_search'),
    url(r'^video_file/(?P<pk>[0-9]+)$', views.VideoFileDownloadView.as_view(),
        name='video_file'),
    url(r'^video_poster/(?P<pk>[0-9]+)$',
        views.VideoFileDownloadView.as_view(),
        name='video_poster'),
    url(r'^video/(?P<pk>[0-9]+)$', views.VideoView.as_view(), name='video'),
    url(r'^video_edit/(?P<pk>[0-9]+)$', views.VideoEditView.as_view(),
        name='video_edit'),
    url(r'^video_create$', views.VideoCreateView.as_view(),
        name='video_create'),
    url(r'^$', RedirectView.as_view(pattern_name='videohosting:video_recent'),
        name='index'),
    url(r'^api/', include(api.router.urls)),
]
