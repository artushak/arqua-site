"use strict";

// TODO: refactor

function emptyFunc() {
}

function sortChildren(selector, sortingFunction) {
  return selector.each(function () {
    const children = $(this).children().get();
    children.sort(sortingFunction);
    $(this).append(children);
  });
};

function loadingFinished() {
}

document.modulesLoaded = {
  'base': false
};

function moduleLoaded(moduleName) {
  document.modulesLoaded[moduleName] = true;
  console.log('Module ' + moduleName + ' loaded');
  for (var moduleName in document.modulesLoaded) {
    if (!document.modulesLoaded[moduleName]) {
      return;
    }
  }

  loadingFinished();
}

function getFormattedDate(date) {
  var month = date.getMonth() + 1;
  var day = date.getDate();
  var hour = date.getHours();
  var min = date.getMinutes();
  var sec = date.getSeconds();

  month = (month < 10 ? "0" : "") + month;
  day = (day < 10 ? "0" : "") + day;
  hour = (hour < 10 ? "0" : "") + hour;
  min = (min < 10 ? "0" : "") + min;
  sec = (sec < 10 ? "0" : "") + sec;

  var str = date.getFullYear() + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;

  return str;
}

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function ajaxErrorDefaultFunc(url) {
  return function (request, status, error) {
    console.warn('AJAX error, status: ' + status + ', url: ' + url);
  };
}

function ajaxGet(url, data, callback, errorCallback, miscParams = {}) {
  if (errorCallback === undefined) {
    errorCallback = ajaxErrorDefaultFunc(url);
  }
  var params = $.extend(
    {
      type: 'GET',
      url: url,
      data: data,
      success: callback,
      error: errorCallback
    },
    miscParams
  );
  $.ajax(params);
}

function ajaxPost(url, data, callback, errorCallback, miscParams = {}) {
  if (errorCallback === undefined) {
    errorCallback = ajaxErrorDefaultFunc(url);
  }
  var params = $.extend(
    {
      type: 'POST',
      url: url,
      data: data,
      headers: {
        'X-CSRFToken': csrftoken
      },
      success: callback,
      error: errorCallback
    },
    miscParams
  );
  $.ajax(params);
}

function ajaxPut(url, data, callback, errorCallback, miscParams = {}) {
  if (errorCallback === undefined) {
    errorCallback = ajaxErrorDefaultFunc(url);
  }
  var params = $.extend(
    {
      type: 'PUT',
      url: url,
      data: data,
      headers: {
        'X-CSRFToken': csrftoken
      },
      success: callback,
      error: errorCallback
    },
    miscParams
  );
  $.ajax(params);
}

function getUserElement(user) {
  if (user == null)
    return $('<span></span>').text('Неизвестный пользователь');
  else
    return $('<a href="' + user.absolute_url + '">' + user.user.username + '</a>');
}


function spoilerOpenClick(e) {
  var spoiler = $(e.target).parent().parent();
  spoiler.find('.spoiler-element, .spoiler-close').show();
  spoiler.find('.spoiler-open').hide();
}

function spoilerCloseClick(e) {
  var spoiler = $(e.target).parent().parent();
  spoiler.find('.spoiler-element, .spoiler-close').hide();
  spoiler.find('.spoiler-open').show();
}

function init() {
  $('.spoiler-close, .spoiler-element').hide();
  $('.spoiler .spoiler-open a').click(spoilerOpenClick);
  $('.spoiler .spoiler-close a').click(spoilerCloseClick);
  $(function () {
    $('form.uniForm').uniform();
  });
  moduleLoaded('base');
}

$(document).ready(init);
