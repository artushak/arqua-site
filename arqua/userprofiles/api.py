# -*- coding: utf-8 -*-
"""Django REST API configuration for `userprofiles`."""
from rest_framework import routers

from userprofiles.models import UserProfileViewSet

router = routers.DefaultRouter()
router.register(r'user-profiles', UserProfileViewSet)
