# -*- coding: utf-8 -*-
"""Django DB models for `userprofiles`."""
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from rest_framework import (
    serializers, viewsets, decorators, permissions, response
)


class UserProfile(models.Model):
    """Additional information about Django `User`."""

    user = models.OneToOneField(User,
                                verbose_name='Пользователь',
                                related_name='profile',
                                on_delete=models.CASCADE)
    subscribers = models.ManyToManyField('self',
                                         verbose_name='Подписчики',
                                         related_name='subscriptions',
                                         blank=True)

    def detail_url(self):
        """Return URL of API method to get user data."""
        return reverse('userprofiles:userprofile-detail', args=[self.pk])

    def subscribe_url(self):
        """Return URL of API method to subscribe to this user."""
        return reverse('userprofiles:userprofile-subscribe', args=[self.pk])

    def unsubscribe_url(self):
        """Return URL of API method to unsubscribe from this user."""
        return reverse('userprofiles:userprofile-unsubscribe', args=[self.pk])

    def subscribers_count(self):
        """Return number of users subscribed to this user."""
        return len(self.subscribers.all())

    def get_absolute_url(self):
        """Return URL of user profile."""
        return reverse('userprofiles:user', args=(self.pk, ))

    def __str__(self):
        """Return string representation."""
        return self.user.username

    class Meta:
        """Metaclass."""

        verbose_name = 'профиль пользователя'
        verbose_name_plural = 'профили пользователей'
        permissions = (
            ('can_subscribe', 'Может подписываться на пользователей'),
        )


def get_user_profile_object_from_user(user):
    """Get `UserProfile` object for user, creating it if needed."""
    try:
        profile = UserProfile.objects.get(user__pk=user.pk)
    except ObjectDoesNotExist:
        profile = UserProfile.objects.create(user=user)
        profile.save()
    return profile


def get_user_profile_current_object(request):
    """Get `UserProfile` object for current user, creating it if needed."""
    return get_user_profile_object_from_user(request.user)


class UserSerializer(serializers.ModelSerializer):
    """Serializer for Django `User`."""

    class Meta:
        """Metaclass."""

        model = User
        fields = (
            'pk', 'username', 'last_name', 'first_name'
        )


class UserProfileSerializer(serializers.ModelSerializer):
    """Serializer for `UserProfile`."""

    user = UserSerializer(read_only=True)
    is_subscribed = serializers.SerializerMethodField(read_only=True)
    absolute_url = serializers.CharField(source='get_absolute_url')

    def get_is_subscribed(self, obj):
        """Return `True` if request user is subscribed to `obj`."""
        request_user = self.context['request'].user
        return bool(obj.subscribers.filter(user__pk=request_user.pk))

    class Meta:
        """Metaclass."""

        model = UserProfile
        fields = (
            'pk', 'user', 'is_subscribed', 'subscribers_count', 'absolute_url'
        )


class UserProfileAPIPermissions(permissions.BasePermission):
    """Permissions for `UserProfile` view set."""

    def has_permission(self, request, view):
        """Return `True` if permission is granted, `False` otherwise."""
        if view.action in ['subscribe', 'unsubscribe']:
            if not request.user or (not request.user.is_authenticated):
                return False
            return request.user.has_perm('userprofiles.can_subscribe')

        return False


class UserProfileViewSet(viewsets.ReadOnlyModelViewSet):
    """View set for Django `User`."""

    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
    permission_classes = (UserProfileAPIPermissions | permissions.IsAdminUser,)

    @decorators.action(detail=True, methods=('post',))
    def subscribe(self, request, pk=None):
        """Subscribe request user to this user."""
        user_profile = self.get_object()
        user_profile.subscribers.add(
            get_user_profile_object_from_user(request.user)
        )
        user_profile.save()
        serializer = self.get_serializer(user_profile)
        return response.Response(serializer.data)

    @decorators.action(detail=True, methods=('post',))
    def unsubscribe(self, request, pk=None):
        """Unsubscribe request user to this user."""
        user_profile = self.get_object()
        user_profile.subscribers.remove(
            get_user_profile_object_from_user(request.user)
        )
        user_profile.save()
        serializer = self.get_serializer(user_profile)
        return response.Response(serializer.data)


class InviteCode(models.Model):
    """Invite code."""

    username = models.CharField('Имя пользователя',
                                max_length=150,
                                blank=True)
    code_text = models.CharField('Код',
                                 max_length=200,
                                 unique=True)

    class Meta:
        """Metaclass."""

        verbose_name = 'инвайт-код'
        verbose_name_plural = 'инвайт-коды'

    def __str__(self):
        """Return string representation."""
        return 'Инвайт-код для {}'.format(self.username or '*')
