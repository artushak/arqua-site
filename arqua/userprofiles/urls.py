# -*- coding: utf-8 -*-
"""Django configuration for application `userprofiles`."""
from django.conf.urls import url, include

from userprofiles import views, api


app_name = 'userprofiles'
urlpatterns = [
    url(r'^user/(?P<pk>[0-9]*)$', views.UserProfileView.as_view(),
        name='user'),
    url(r'^user_current$', views.view_user_profile_current,
        name='user_current'),
    url(r'^api/', include(api.router.urls)),
]
