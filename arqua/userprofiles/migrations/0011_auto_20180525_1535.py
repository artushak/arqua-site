# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-05-25 15:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userprofiles', '0010_auto_20171201_0955'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invitecode',
            name='code_text',
            field=models.CharField(max_length=200, unique=True, verbose_name='Код'),
        ),
    ]
