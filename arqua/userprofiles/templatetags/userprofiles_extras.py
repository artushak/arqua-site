# -*- coding: utf-8 -*-
"""Django template tags and filters for `userprofiles`."""
from django.template import Library


register = Library()


@register.filter()
def is_subscribed(subscriber, target):
    """
    Return `True` if `subscriber` is subscribed to `target`.

    `subscriber` should be Django `User`, `target should be `UserProfile`.
    """
    return bool(target.subscribers.filter(user__pk=subscriber.pk))
