# -*- coding: utf-8 -*-
"""Django forms for `userprofiles`."""
from django import forms
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.core.exceptions import ObjectDoesNotExist
from django_registration import validators
from django_registration.forms import RegistrationForm
from crispy_forms.layout import Submit

from arqua.utils import form_with_helper
from userprofiles.models import UserProfile, InviteCode


@form_with_helper
class RegistrationWithInviteForm(RegistrationForm):
    """Form for registration with invite and helper."""

    error_messages = {
        'password_mismatch': 'Пароли не совпадают',
        'invite_code_wrong':
            'Инвайт-код не найден или не соответствует имени пользователя',
    }
    email = forms.EmailField(
        help_text='Адрес электронной почты.',
        required=False,
        validators=[
            validators.validate_confusables_email,
        ]
    )
    invite_code = forms.CharField(
        label='Инвайт-код',
        widget=forms.PasswordInput,
        strip=False,
        help_text='Инвайт-код (если он у вас есть).',
    )

    def clean_invite_code(self):
        try:
            invite_code_obj = InviteCode.objects.get(
                code_text=self.cleaned_data.get('invite_code')
            )
        except ObjectDoesNotExist:
            raise forms.ValidationError(
                self.error_messages['invite_code_wrong'],
                code='invite_code_wrong',
            )
        if invite_code_obj.username != '':
            if invite_code_obj.username != self.cleaned_data.get('username'):
                raise forms.ValidationError(
                    self.error_messages['invite_code_wrong'],
                    code='invite_code_wrong',
                )
        self.invite_code_obj = invite_code_obj

    def modify_helper(self, helper):
        """Modify `crispy_forms` helper."""
        helper.form_class = 'login-form'
        helper.add_input(
            Submit('submit', 'Зарегистрироваться', css_class="btn-primary")
        )
        return helper

    def save(self, commit=True):
        user = super(RegistrationWithInviteForm, self).save(commit)
        user_profile = UserProfile.objects.create(user=user)
        user_profile.save()
        self.invite_code_obj.delete()
        return user


@form_with_helper
class AuthenticationFormWithHelper(AuthenticationForm):
    """Form for authentication with helper."""

    def modify_helper(self, helper):
        """Modify `crispy_forms` helper."""
        helper.form_class = 'login-form'
        helper.add_input(
            Submit('submit', 'Войти', css_class="btn-primary")
        )
        return helper


@form_with_helper
class PasswordChangeFormWithHelper(PasswordChangeForm):
    """Form for password change with helper."""

    def modify_helper(self, helper):
        """Modify `crispy_forms` helper."""
        helper.form_class = 'login-form'
        helper.add_input(
            Submit('submit', 'Сменить', css_class="btn-primary")
        )
        return helper
