# -*- coding: utf-8 -*-
"""Django views for `userprofiles`."""
from django.contrib.auth import views as auth_views
from django.shortcuts import redirect
from django.views.generic import DetailView
from django.urls import reverse, reverse_lazy
from django_registration.backends.one_step import views as registration_views

from userprofiles.apps import page_path_base
from userprofiles.forms import (
    RegistrationWithInviteForm, AuthenticationFormWithHelper,
    PasswordChangeFormWithHelper
)
from userprofiles.models import UserProfile, get_user_profile_current_object


class UserProfileView(DetailView):
    """User profile view."""

    model = UserProfile

    template_name = 'userprofiles/user.html'

    def get_context_data(self, **kwargs):
        """Get context data for view."""
        context = super(UserProfileView, self).get_context_data(**kwargs)
        context['can_subscribe'] = self.request.user.has_perm(
            'userprofiles.can_subscribe'
        )
        page_name = 'пользователь:{}'.format(context['object'].user.username)
        page_url = reverse('userprofiles:user', args=(context['object'].pk,))
        context['page_path'] = page_path_base + [(page_name, page_url)]
        return context


def view_user_profile_current(request):
    """View current user profile."""
    profile = get_user_profile_current_object(request)
    return redirect('userprofiles:user', profile.pk)


class RegistrationView(registration_views.RegistrationView):
    """Login view."""

    form_class = RegistrationWithInviteForm
    template_name = 'userprofiles/register.html'
    success_url = reverse_lazy('userprofiles:user_current')

    def get_context_data(self, **kwargs):
        """Get context data for view."""
        context = super(RegistrationView, self).get_context_data(**kwargs)
        page_name = 'регистрация'
        page_url = reverse('register')
        context['page_path'] = page_path_base + [(page_name, page_url)]
        return context


class LoginView(auth_views.LoginView):
    """Login view."""

    form_class = AuthenticationFormWithHelper
    template_name = 'userprofiles/login.html'

    def get_context_data(self, **kwargs):
        """Get context data for view."""
        context = super(LoginView, self).get_context_data(**kwargs)
        page_name = 'вход'
        page_url = reverse('login')
        context['page_path'] = page_path_base + [(page_name, page_url)]
        return context


class LogoutView(auth_views.LogoutView):
    """Logout view."""


class PasswordChangeView(auth_views.PasswordChangeView):
    """Password change view."""

    form_class = PasswordChangeFormWithHelper
    template_name = 'userprofiles/password_change_form.html'

    def get_context_data(self, **kwargs):
        """Get context data for view."""
        context = super(PasswordChangeView, self).get_context_data(**kwargs)
        page_name = 'смена пароля'
        page_url = reverse('password_change')
        context['page_path'] = page_path_base + [(page_name, page_url)]
        return context


class PasswordChangeDoneView(auth_views.PasswordChangeDoneView):
    """Password change done view."""

    template_name = 'userprofiles/password_change_done.html'

    def get_context_data(self, **kwargs):
        """Get context data for view."""
        context = super(PasswordChangeDoneView, self).get_context_data(
            **kwargs
        )
        page_name = 'смена пароля'
        context['page_path'] = page_path_base + [(page_name, None)]
        return context
