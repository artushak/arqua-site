# -*- coding: utf-8 -*-
"""Django administration configuration for `userprofiles`."""
from django.contrib import admin

from userprofiles.models import UserProfile, InviteCode


class UserProfileAdmin(admin.ModelAdmin):
    """Administration class for `UserProfile`."""

    model = UserProfile


class InviteCodeAdmin(admin.ModelAdmin):
    """Administration class for `InviteCode`."""

    model = InviteCode


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(InviteCode, InviteCodeAdmin)
