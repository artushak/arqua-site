# -*- coding: utf-8 -*-
"""Django application `userprofiles`."""
from django.apps import AppConfig
from django.urls import reverse_lazy


class UserprofilesConfig(AppConfig):
    """Django application configuration for `userprofiles`."""

    name = 'userprofiles'
    verbose_name = 'человечки'


page_path_base = [
    ('тчк', reverse_lazy('index')),
    ('человечки', None),
]
