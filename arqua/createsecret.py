# -*- coding: utf-8 -*-
"""Script to create Django secret key."""
import secrets


def get_secret_key():
    """Create secret key using `secrets` and return it."""
    return secrets.token_hex(64)


if __name__ == '__main__':
    print(get_secret_key())
