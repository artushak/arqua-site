# -*- coding: utf-8 -*-
"""Django site settings, mostly loaded from .env."""
import os

import dj_database_url
import dotenv


def parse_bool(string):
    """Parse string and return bool."""
    return string.upper() == 'TRUE'


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

dotenv.load_dotenv(os.path.join(BASE_DIR, '.env'))

SECRET_KEY = os.getenv('SECRET_KEY')

DEBUG = parse_bool(os.getenv('DEBUG'))

ALLOWED_HOSTS_STR = os.getenv('ALLOWED_HOSTS')
if ALLOWED_HOSTS_STR is not None:
    ALLOWED_HOSTS = ALLOWED_HOSTS_STR.split(';')
else:
    ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'crispy_forms',
    'rest_framework',
    'rest_framework.authtoken',
    'django_filters',
    'private_storage',
    'userprofiles',
    'telechat',
    'videohosting',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'arqua.urls'

TEMPLATES_DEBUG = parse_bool(os.getenv('TEMPLATES_DEBUG'))

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'debug': TEMPLATES_DEBUG
        },
    },
]

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

CRISPY_TEMPLATE_PACK = 'uni_form'  # TODO
CRISPY_FAIL_SILENTLY = parse_bool(os.getenv('CRISPY_FAIL_SILENTLY'))

WSGI_APPLICATION = 'arqua.wsgi.application'

DATABASES = {
    'default': dj_database_url.config(),
}

DATABASE_INIT_COMMAND = os.getenv('DATABASE_INIT_COMMAND', None)
if DATABASE_INIT_COMMAND is not None:
    DATABASES['default']['OPTIONS'] = {}
    DATABASES['default']['OPTIONS']['init_command'] = DATABASE_INIT_COMMAND

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME':
        ('django.contrib.auth.password_validation.'
         'UserAttributeSimilarityValidator'),
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = os.getenv('LANGUAGE_CODE')

TIME_ZONE = os.getenv('TIME_ZONE')

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOGIN_REDIRECT_URL = '/'

LOGOUT_REDIRECT_URL = '/'

ATOMIC_REQUESTS = parse_bool(os.getenv('ATOMIC_REQUESTS'))

STATIC_ROOT = os.getenv('STATIC_ROOT')

STATIC_URL = os.getenv('STATIC_URL')

MEDIA_ROOT = os.getenv('MEDIA_ROOT')

MEDIA_URL = os.getenv('MEDIA_URL')

PRIVATE_STORAGE_ROOT = os.getenv('PRIVATE_STORAGE_ROOT')

PRIVATE_STORAGE_SERVER = os.getenv('PRIVATE_STORAGE_SERVER')

PRIVATE_STORAGE_INTERNAL_URL = os.getenv('PRIVATE_STORAGE_INTERNAL_URL')


SECURE_HSTS_SECONDS = int(os.getenv('SECURE_HSTS_SECONDS'))

X_FRAME_OPTIONS = os.getenv('X_FRAME_OPTIONS')

SECURE_CONTENT_TYPE_NOSNIFF = parse_bool(
    os.getenv('SECURE_CONTENT_TYPE_NOSNIFF')
)

SECURE_BROWSER_XSS_FILTER = parse_bool(os.getenv('SECURE_BROWSER_XSS_FILTER'))

SESSION_COOKIE_SECURE = parse_bool(os.getenv('SESSION_COOKIE_SECURE'))

CSRF_COOKIE_SECURE = parse_bool(os.getenv('CSRF_COOKIE_SECURE'))

SECURE_HSTS_PRELOAD = parse_bool(os.getenv('SECURE_HSTS_PRELOAD'))

SECURE_HSTS_INCLUDE_SUBDOMAINS = parse_bool(
    os.getenv('SECURE_HSTS_INCLUDE_SUBDOMAINS')
)

SECURE_SSL_REDIRECT = parse_bool(os.getenv('SECURE_SSL_REDIRECT'))


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'formatters': {
        'django.server': {
            '()': 'django.utils.log.ServerFormatter',
            'format': '{message}',
            'style': '{',
        }
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
        },
        'django.server': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'django.server',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        'arqua': {
            'handlers': ['console', 'django.server'],
            'level': 'INFO',
        },
        'django.server': {
            'handlers': ['django.server'],
            'level': 'INFO',
            'propagate': False,
        },
    }
}
