# -*- coding: utf-8 -*-
"""Common utilitary functions."""
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit


def form_with_helper(cls):
    """Add `crispy_forms` helper to form class."""
    def get_helper(self):
        helper = FormHelper(self)
        return self.modify_helper(helper)

    def set_fields(self):
        pass

    def modify_helper(self, helper):
        helper.add_input(
            Submit('submit', 'Сохранить', css_class="btn-primary"))
        return helper

    def get_new_constructor(old_constructor):
        def constructor(self, *args, **kwargs):
            old_constructor(self, *args, **kwargs)
            self.set_fields()
            self.helper = self.get_helper()
        return constructor

    def wrap(cls):
        if not hasattr(cls, 'set_fields'):
            cls.set_fields = set_fields
        if not hasattr(cls, 'modify_helper'):
            cls.modify_helper = modify_helper
        cls.get_helper = get_helper
        cls.__init__ = get_new_constructor(cls.__init__)
        return cls

    return wrap(cls)
