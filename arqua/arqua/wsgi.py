# -*- coding: utf-8 -*-
"""Django WSGI script."""
import os
import sys

from django.core.wsgi import get_wsgi_application


BASE_DIR = os.path.dirname(os.path.abspath(__file__))

sys.path.insert(0, os.path.join(os.environ['HOME'], '.local/bin/'))
sys.path.insert(0, os.path.join(BASE_DIR, '../'))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "arqua.settings")

application = get_wsgi_application()
