# -*- coding: utf-8 -*-
"""Django URL root configuration."""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import RedirectView
import private_storage.urls

from userprofiles import views as userprofiles_views


urlpatterns = [
    url(r'^userprofiles/', include('userprofiles.urls')),
    url(r'^videohosting/', include('videohosting.urls')),
    url(r'^telechat/', include('telechat.urls')),
    url(r'^admin/', admin.site.urls),

    url(
        r'^accounts/register/$',
        userprofiles_views.RegistrationView.as_view(),
        name='register'
    ),
    url(
        r'^accounts/login/$',
        userprofiles_views.LoginView.as_view(),
        name='login'
    ),
    url(
        r'^accounts/logout/$',
        userprofiles_views.LogoutView.as_view(),
        name='logout'
    ),
    url(
        r'^accounts/password-change/$',
        userprofiles_views.PasswordChangeView.as_view(),
        name='password_change'
    ),
    url(
        r'^accounts/password-change/done$',
        userprofiles_views.PasswordChangeDoneView.as_view(),
        name='password_change_done'
    ),
    url(r'^api-auth/', include('rest_framework.urls')),
]
urlpatterns += [
    url(r'^$', RedirectView.as_view(pattern_name='videohosting:video_recent'),
        name='index')
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += [
    url('^private-media/', include(private_storage.urls)),
]
