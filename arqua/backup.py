# -*- coding: utf-8 -*-
"""Script for creating and managing backups."""
import os
import datetime
import re
import subprocess


def get_backup_filename(backup_date):
    """Get backup file name from date."""
    return 'backup-{}.json'.format(backup_date.isoformat())


def get_backup_date(filename):
    """Get backup date from file name."""
    match = re.match(r'^backup-(\d{4})-(\d{2})-(\d{2})\.json$', filename)
    if match is not None:
        return datetime.date(
            int(match.group(1)),
            int(match.group(2)),
            int(match.group(3))
        )


BACKUP_DIR = '../backup/'
BACKUP_PERIOD = 10
PYTHON_EXECUTABLE = 'python'
MANAGE_FILE = './timetablesystem/manage.py'
MANAGE_PARAMS = [
    'dumpdata', '--natural-foreign', '--natural-primary',
    '--exclude=contenttypes', '--exclude=auth.Permission'
]


def run(
    backup_dir=BACKUP_DIR, backup_period=BACKUP_PERIOD,
    python_executable=PYTHON_EXECUTABLE, manage_file=MANAGE_FILE,
    manage_params=MANAGE_PARAMS
):
    """Run backup script."""
    os.makedirs(backup_dir, exist_ok=True)

    today = datetime.date.today()

    backup_dir_list = os.listdir(backup_dir)
    for backup_dir_file in backup_dir_list:
        backup_file_date = get_backup_date(backup_dir_file)
        if backup_file_date is not None:
            backup_file_timedelta = backup_file_date - today
            if backup_file_timedelta.days < -backup_period:
                os.remove(os.path.join(backup_dir, backup_dir_file))

    backup_filename = os.path.join(backup_dir, get_backup_filename(today))

    subprocess.run(
        [python_executable, manage_file] + manage_params
        + ['--output', backup_filename]
    )


if __name__ == "__main__":
    run()
